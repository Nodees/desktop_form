import sys
from PyQt5.QtWidgets import QApplication
from views.main_view import MainForm

app = QApplication(sys.argv)
main = MainForm()
main.show()
sys.exit(app.exec_())