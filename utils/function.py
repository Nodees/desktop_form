from PyQt5.QtWidgets import QMessageBox

from utils.connection import connection, conn

def users_list():
    query = conn.execute("SELECT * FROM users")
    query = conn.fetchall()

    return query


def alert_dialog( msg: str, title: str):

    msgBox = QMessageBox()

    if title == 'success':
        type_dialog = QMessageBox.Information
        type_button = QMessageBox.Ok
    else:
        type_dialog = QMessageBox.Warning
        type_button = QMessageBox.Close

    msgBox.setIcon(type_dialog)
    msgBox.setText(msg)
    msgBox.setWindowTitle(title)
    msgBox.setStandardButtons(type_button)
    msgBox.exec()