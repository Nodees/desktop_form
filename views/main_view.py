import os
import sys

from models.users import user

from utils.connection import conn, connection
from utils.function import users_list

from views.update_form import update_form
from utils.function import alert_dialog

from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtWidgets import (
    QMainWindow,
    QHBoxLayout,
    QLabel,
    QVBoxLayout,
    QWidget,
    QLineEdit,
    QComboBox,
    QSizePolicy,
    QSpacerItem,
    QPushButton,
    QTableWidget,
    QTableWidgetItem,
    QHeaderView,
    QMessageBox,
)


class MainForm(QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Form")
        self.setGeometry(20, 20, 800, 400)

        space = QSpacerItem(0, 0, QSizePolicy.Fixed, QSizePolicy.Expanding)

        self.btn_add = QPushButton()
        self.btn_add.setText('Add')
        self.btn_add.clicked.connect(self._on_add)

        self.label_name = QLabel()
        self.label_name.setText("Name")
        self.edt_name = QLineEdit()

        self.label_salary = QLabel()
        self.label_salary.setText("Salary")
        self.edt_salary = QLineEdit()
        self.edt_salary.setValidator(QDoubleValidator())

        self.label_gender = QLabel()
        self.label_gender.setText("Gender")
        self.edt_gender = QComboBox()
        self.edt_gender.addItems(["Male", "Female"])

        self.table = QTableWidget()
        self.table.setColumnCount(6)
        self.table.setHorizontalHeaderLabels(['Name', 'Salary', 'Gender', 'Birthday', '', ''])
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table.setRowCount(0)

        self.label_birthday = QLabel()
        self.label_birthday.setText("Birthday")
        self.edt_birthday = QLineEdit()

        self.layout = QVBoxLayout()
        self.top = QHBoxLayout()
        self.down = QHBoxLayout()

        self.top.addWidget(self.label_name)
        self.top.addWidget(self.edt_name)
        self.top.addWidget(self.label_gender)
        self.top.addWidget(self.edt_gender)

        self.down.addWidget(self.label_salary)
        self.down.addWidget(self.edt_salary)
        self.down.addWidget(self.label_birthday)
        self.down.addWidget(self.edt_birthday)

        self.layout.addLayout(self.top)
        self.layout.addLayout(self.down)

        self.layout.addWidget(self.btn_add)

        self.layout.addWidget(self.table)

        self.layout.addItem(space)

        self.panel = QWidget()
        self.panel.setLayout(self.layout)

        self.setCentralWidget(self.panel)

        self.populate()

    def _on_add(self):
        obj = user(self.edt_name.text(),
                   self.edt_gender.currentText(),
                   self.edt_salary.text(),
                   self.edt_birthday.text())

        try:
            conn.execute('insert into users(name, salary, gender, birthday) values(%s, %s, %s, %s)',
                         (obj.name, float(obj.salary), obj.gender, obj.birthday))

            length = self.table.rowCount()
            self.table.insertRow(length)

            self.edt_name.clear()
            self.edt_salary.clear()
            self.edt_birthday.clear()

            name = QTableWidgetItem()
            name.setTextAlignment(4)
            name.setText(obj.name)

            salary = QTableWidgetItem()
            salary.setTextAlignment(4)
            salary.setText(obj.salary)

            gender = QTableWidgetItem()
            gender.setTextAlignment(4)
            gender.setText(obj.gender)

            birthday = QTableWidgetItem()
            birthday.setTextAlignment(4)
            birthday.setText(obj.birthday)

            self.table.setItem(length, 0, name)
            self.table.setItem(length, 1, salary)
            self.table.setItem(length, 2, gender)
            self.table.setItem(length, 3, birthday)

            connection.commit()

            alert_dialog('Successfully Added', 'success')
        except Exception as erro:
            alert_dialog(f'Insert Error, check that all fields are filled in correctly \n log: {erro}', 'Error')

    def populate(self):

        self.users = users_list()
        self.table.setRowCount(len(self.users))

        for line, valor in enumerate(self.users):
            name = QTableWidgetItem()
            name.setTextAlignment(4)
            name.setText(valor[1])

            salary = QTableWidgetItem()
            salary.setTextAlignment(4)
            salary.setText(str(valor[2]))

            gender = QTableWidgetItem()
            gender.setTextAlignment(4)
            gender.setText(valor[3])

            birthday = QTableWidgetItem()
            birthday.setTextAlignment(4)
            birthday.setText(str(valor[4]))

            button_delete = self._on_delete('Delete')
            button_update = self._on_update('Update')

            button_delete.clicked.connect(
                lambda clicked, data=valor[0]: self.delete(data)
            )

            button_update.clicked.connect(
                lambda clicked, data={
                    'pk': int(valor[0]),
                    'name': str(valor[1]),
                    'salary': str(valor[2]),
                    'gender': str(valor[3]),
                    'birthday': str(valor[4])
                }: self.update(data)
            )

            self.table.setItem(line, 0, name)
            self.table.setItem(line, 1, salary)
            self.table.setItem(line, 2, gender)
            self.table.setItem(line, 3, birthday)
            self.table.setCellWidget(line, 4, button_delete)
            self.table.setCellWidget(line, 5, button_update)

    def _on_delete(self, text_button):
        button = QPushButton()
        button.setText(text_button)
        button.setProperty('type', 'normal')

        return button

    def _on_update(self, text_button):
        button = QPushButton()
        button.setText(text_button)
        button.setProperty('type', 'normal')

        return button

    def delete(self, data: int):
        try:
            conn.execute(f'delete from users where id = {data}')
            connection.commit()
            alert_dialog('Deleted', 'success')

            python = sys.executable
            os.execl(python, python, *sys.argv)

        except Exception as erro:
            alert_dialog(f'Error \n{erro}', 'Error')

    def update(self, data: dict):
        self.my_dialog = update_form(self, **data)
        self.my_dialog.exec_()
