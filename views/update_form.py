import os
import sys

from utils.function import alert_dialog
from PyQt5.QtGui import QDoubleValidator
from models.users import user
from utils.connection import conn, connection
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QLineEdit, QHBoxLayout,\
    QComboBox, QSpacerItem, QSizePolicy, QPushButton



class update_form(QDialog):

    def __init__(self, parent=None, **kwargs):
        super().__init__(parent=parent)

        self.data = kwargs

        self.setWindowTitle("Form Update")
        self.setGeometry(10, 10, 400, 200)

        space = QSpacerItem(0, 0, QSizePolicy.Fixed, QSizePolicy.Expanding)

        self.btn_update = QPushButton()
        self.btn_update.setText('Update')
        self.btn_update.clicked.connect(self._on_update)

        self.label_name = QLabel()
        self.label_name.setText("Name")
        self.update_name = QLineEdit(self.data.get("name"))

        self.label_salary = QLabel()
        self.label_salary.setText("Salary")
        self.update_salary = QLineEdit(self.data.get("salary"))
        self.update_salary.setValidator(QDoubleValidator())

        self.label_gender = QLabel()
        self.label_gender.setText("Gender")
        self.update_gender = QComboBox()
        self.update_gender.addItems(["Male", "Female"])
        self.update_gender.setCurrentText(self.data.get('gender'))

        self.label_birthday = QLabel()
        self.label_birthday.setText("Birthday")
        self.update_birthday = QLineEdit(self.data.get("birthday"))

        self.layout = QVBoxLayout()
        self.top = QHBoxLayout()
        self.down = QHBoxLayout()

        self.top.addWidget(self.label_name)
        self.top.addWidget(self.update_name)
        self.top.addWidget(self.label_gender)
        self.top.addWidget(self.update_gender)

        self.down.addWidget(self.label_salary)
        self.down.addWidget(self.update_salary)
        self.down.addWidget(self.label_birthday)
        self.down.addWidget(self.update_birthday)

        self.layout.addLayout(self.top)
        self.layout.addLayout(self.down)

        self.layout.addWidget(self.btn_update)

        self.layout.addItem(space)

        self.setLayout(self.layout)

    def _on_update(self):
        obj = user(self.update_name.text(),
                   self.update_gender.currentText(),
                   self.update_salary.text(),
                   self.update_birthday.text())

        sql = f"UPDATE users SET name = %s, salary = %s, gender = %s, birthday = %s WHERE id = %s"

        try:
            conn.execute(sql, (obj.name, obj.salary, obj.gender, obj.birthday, self.data.get('pk')))
            connection.commit()

            alert_dialog('Update Successfully', 'success')

            python = sys.executable
            os.execl(python, python, *sys.argv)

        except Exception as erro:
            alert_dialog(f'Update error\n{erro}', 'error')
